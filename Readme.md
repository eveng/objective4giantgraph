1. Final result location:
    - Author to Hash: da0:/export/data/play/Graphic_authorship/hash.author.gz
    - Graph of Hash to Hsh: da0:/export/data/play/Graphic_authorship/Merged_graph_for_all.gz
2. Program source code folder: da0: /export/data/play/Graphic_authorship/
    - Merge_two_subgraph.py:  Main file
    - revised_connectpbyCID.perl: generate transitive closure graph
    - /clean_self_link.py: remove duplicates in result
